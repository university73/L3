    # Appel
	  li	  $a0, 0x1f
	  jal	  compte
    # Affichage
	  move 	$a0, $v0
	  li	  $v0, 1
	  syscall
    #Fin du programme
	  li	  $v0, 10
	  syscall 

compte:
	# Tableau d'activation
	  sw	  $fp, -4($sp)
	  sw	  $ra, -8($sp)
	  addi	$fp, $sp, -4
	  addi	$sp, $sp, -12
	# Test
	  beqz	$a0, .end
	  sw	  $a0, -8($fp)
	  sra	  $a0, $a0, 1
	  jal	  compte
	  lw	  $a0, -8($fp)
	  andi	$t0, $a0, 1
	  add 	$v0, $v0, $t0
	  b 	.compte_ret
	.end:
	  li	  $v0, 0	
	.compte_ret:
	# Nettoyage
	  addi	$sp, $fp, 4
	  lw	  $fp, -4($sp)
	  lw	  $ra, -8($sp)
	  jr 	  $ra	
	
