	li	  $a0, 2		# x
	
	li	  $t0, 0		# y
	li	  $v0, 1 		# z
S:
	bge 	$t1, $a0, E # y >= x
	sll	  $v0, $v0, 1	# z = z * 2
	addi	$a1, $a1, 1	# y = y + 1
	b     S
E:
	jr	  $ra
